#include <cassert>
#include <iostream>
#include <vector>

const std::string   hello_world { "Hello World!" } ;

struct MyCollection {
    using list_t = std::vector<std::string> ;
    list_t  items_ ;

    MyCollection () : items_ { hello_world } { /* NO-OP */ }

    list_t::iterator begin () { return items_.begin (); }
    list_t::iterator end   () { return items_.end (); }
    list_t::const_iterator begin () const { return items_.begin (); }
    list_t::const_iterator end   () const { return items_.end (); }
} ;

struct MyCollection2 {
    std::vector<std::string>    items_ ;

    MyCollection2 () : items_ { hello_world } { /* NO-OP */ }

    decltype (items_.begin  ()) begin () { return items_.begin (); }
    decltype (items_.end    ()) end   () { return items_.end (); }
    decltype (items_.cbegin ()) begin () const { return items_.begin (); }
    decltype (items_.cend   ()) end   () const { return items_.end (); }
} ;

struct MyCollection3 {
    std::vector<std::string>    items_ ;

    MyCollection3 () : items_ { hello_world } { /* NO-OP */ }

    auto begin () { return items_.begin (); }
    auto end   () { return items_.end (); }
    auto begin () const { return items_.begin (); }
    auto end   () const { return items_.end (); }
} ;


int main () {
    MyCollection C;

    for (auto const &v : C) {
        auto sz = v.size () ;
        assert (sz == hello_world.size ()) ;    // `v.size ()` Completion success
    }
    MyCollection2   C2;

    for (auto const &v : C2) {
        auto sz = v.size () ;                   // `v.size ()` Completion success
        assert (sz == hello_world.size ()) ;
    }
    MyCollection3   C3;

    for (auto const &v : C3) {
        auto sz = v.size () ;                   // `v.size ()` Completion fail
        assert (sz == hello_world.size ()) ;
    }
    return 0;
}
