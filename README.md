# CLionTest - CLion's completion feature test

CLion 2016.2.2 EAP fails to resolve `MyCollection3` case.

## CLion
```text
CLion 2016.2.2 EAP
Build #CL-162.1889.3, built on August 23, 2016
JRE: 1.8.0_76-release-b216 x86_64
JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
```
